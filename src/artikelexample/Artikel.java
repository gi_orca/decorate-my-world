package artikelexample;

import java.util.GregorianCalendar;

/**
 *Artikel is a Simple Object to artikelexample an item in eg. an online shop
 *
 */
public interface Artikel {
    /**
     * used to get the price
     *
     * @return double containing the price
     */
    double getPreis();

    /**
     * used to get the deliverDate
     *
     * @return a GregorianCalendar representing the earliest DeliveryDate
     */
    GregorianCalendar getEarliestDeliveryDate();

    String getName();

    /* Considered creating default implementation(java8) of toString method to avoid code duplication
     in ArtikelDecorator and BasisArtikel but since multiple implementations each class would be required
     to provide its own application to prevent ambiguity deafeating the purpose.
     */
}
