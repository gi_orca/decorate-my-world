package artikelexample;

public class SkontoDecorator extends ArtikelDecorator {
    public SkontoDecorator(Artikel artikel) {
        super(artikel);
    }

    /**
     * returns the price
     * @return
     */
    @Override
    public double getPreis() {
        return artikel.getPreis() * 0.93;
    }

}
