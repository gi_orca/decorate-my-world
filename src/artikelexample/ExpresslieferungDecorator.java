package artikelexample;

import java.util.GregorianCalendar;

/**
 * Represents priority shipping
 * if shipping is already faster than
 * {@link ExpresslieferungDecorator#lieferdauer}
 * can be used multiple times but doesn't have additional effect
 */
public class ExpresslieferungDecorator extends ArtikelDecorator {
    public ExpresslieferungDecorator(Artikel artikel) {
        super(artikel);
        this.lieferdauer = 48;
    }


    public final int lieferdauer;

    /**
     * Gets the earliest DeliveryDate either now+ {@link ExpresslieferungDecorator#lieferdauer}
     * or the original if that is faster
     * @return
     */
    @Override
    public GregorianCalendar getEarliestDeliveryDate() {
        //initializes with current time
        GregorianCalendar earliest = new GregorianCalendar();
        //add the lieferdauer propterty to it
        earliest.add(GregorianCalendar.HOUR_OF_DAY, this.lieferdauer);
        //get the decorated earliest and compare it
        GregorianCalendar liefer = artikel.getEarliestDeliveryDate();
        if (liefer.after(earliest)){
            liefer = earliest;
        }
        //return the earlier of those
        return liefer;
    }
}
