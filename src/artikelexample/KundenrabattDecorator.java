package artikelexample;

/**
 * Used to add a Flat {@link KundenrabattDecorator#rabatt} percent discount to the price
 * can be used multiple times, applying its discount everytime
 */
public class KundenrabattDecorator extends ArtikelDecorator {
    private final double rabatt;


    public KundenrabattDecorator(Artikel artikel) {
        super(artikel);
        this.rabatt = 10.0;
    }

    /**
     * Returns a discounted price
     * @return
     */
    @Override
    public double getPreis() {
        return artikel.getPreis()*(100-this.rabatt)/100;
    }
}
