package artikelexample;

import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;

public abstract class ArtikelDecorator implements Artikel {
    protected Artikel artikel;

    public ArtikelDecorator(Artikel artikel) {
        this.artikel = artikel;
    }

    @Override
    public double getPreis() {
        return artikel.getPreis();
    }

    @Override
    public  GregorianCalendar getEarliestDeliveryDate(){
        return artikel.getEarliestDeliveryDate();
    }

    @Override
    public String getName(){
        return artikel.getName();
    }

    @Override
    public String toString(){
        StringBuilder sb = new StringBuilder();
        sb.append(this.getName());
        sb.append(": ");
        sb.append("Preis: "+this.getPreis());
        sb.append(" Frühesete Lieferung: " +new SimpleDateFormat().format(this.getEarliestDeliveryDate().getTime()));
        return sb.toString();
        //does not work returns wrong date
      //return artikel.toString();
    }


}
