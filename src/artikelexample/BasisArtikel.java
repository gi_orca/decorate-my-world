package artikelexample;

import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;

public class BasisArtikel implements Artikel {
    private double preis;
    private int lieferzeit;
    private String name;

    public BasisArtikel(String name, double preis, int lieferzeit) {
        this.preis = preis;
        this.lieferzeit = lieferzeit;
        this.name = name;
    }

    @Override
    public double getPreis() {
        return this.preis;
    }

    /**
     * Adds the Objects lieferzeit property to the current Date
     *
     * @return Returns the ealiest Delivery Date
     */
    @Override
    public GregorianCalendar getEarliestDeliveryDate() {
        GregorianCalendar date = new GregorianCalendar();
        date.add(GregorianCalendar.DAY_OF_MONTH, this.lieferzeit);
        return date;
    }

    /**
     * used to get the Article Name
     * @return
     */
    @Override
    public String getName() {
        return name;
    }

    /**
     * Sets a new Price no Validity check!!
     *
     * @param preis
     */
    public void setPreis(double preis) {
        //todo Validity Check
        this.preis = preis;
    }

    /**
     * Gets the DeliveryTime value in Days
     * @return
     */
    public int getLieferzeit() {
        return lieferzeit;
    }

    /**
     * Set the DeliverytimeTime value in Days
     * @param lieferzeit
     */
    public void setLieferzeit(int lieferzeit) {
        this.lieferzeit = lieferzeit;
    }

    @Override
    public String toString(){
        StringBuilder sb = new StringBuilder();
        sb.append(this.getName());
        sb.append(": ");
        sb.append("Preis: "+this.getPreis());
        sb.append(" Frühesete Lieferung: " +new SimpleDateFormat().format(this.getEarliestDeliveryDate().getTime()));
        return sb.toString();
    }
}
