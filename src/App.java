import artikelexample.*;

import java.util.ArrayList;
import java.util.Scanner;

public class App {
    Scanner scan = new Scanner(System.in);
    ArrayList<Artikel> artikelListe = new ArrayList<>();
    Artikel selected;
    Boolean express = false;
    Boolean kundenRabatt= false;
    Boolean skonto= false;
    public static void main(String[] args){
        App app = new App();
    }

    public App(){
        setup();

        output();
        try{
           while(true){
            outputActions();
            selectAction();
           }
        }
        catch (NumberFormatException e){
            System.out.println("Abgebrochen");
        }
        catch (IndexOutOfBoundsException e){
            System.out.println("Keine Gültige Postion");
        }



    }

    /**
     * Creates DummyData
     */
    public void setup(){
        artikelListe.add(new BasisArtikel("Kaugummi", 99, 1));
        artikelListe.add(new BasisArtikel("Lichtschwert", 5000, 5));
        artikelListe.add(new BasisArtikel("Buch des absoluten Wissens", 99999, 3));
        artikelListe.add(new BasisArtikel("Gutscheincode", 2000, 0));
        artikelListe.add(new ExpresslieferungDecorator(artikelListe.get(1)));
    }

    /**
     * Displays a List of the available Articles
     */
    public void output(){
        Artikel a = null;
        for (int i = 0; i < artikelListe.size() ; i++) {
            a= artikelListe.get(i);
            System.out.println(""+(i+1)+ " "+a);
        }

    }

    private void outputActions(){
        String[] actions = {
                "neuen Artikel anlegen",
                "Artikel auswählen",
                "Expresslieferung zum ausgewählten Artikel hinzufügen",
                "Kundenrabatt zum ausgewählten Artikel hinzufügen",
                "Skonto zum ausgewählten Artikel hinzufügen",
                "Bezahlen",
                "Beenden"
        };
        int l = actions.length;
        System.out.println("---------------------------------------------------------");
        System.out.println("Gewählter Artikel: "+selected +(this.express?" Express":"")
        +(this.kundenRabatt?" Rabatt":"") +(this.skonto?" Skonto":"")
        );
        for (int i =0;i <l; i++ ){
            System.out.println(""+(i+1)+ " "+actions[i]);
        }
    }

    private void selectAction(){
        String entry = getEntry("Wählen sie entsprechende Zahl aus: ");
        try {
        switch (entry){
            case "1":this.artikelAnlegen();break;
            case "2":this.selectArticle();break;
            case "3":this.expressLiefern(this.selected);break;
            case "4":this.kundenRabattAnwenden(this.selected);break;
            case "5":this.skontoAnwenden(this.selected);break;
            case "6":this.checkout();break;
            case "7":this.exit();break;
        }
        }
        catch (NullPointerException e){

        }
    }


    private void checkout() {
        System.out.println("--------------------------------------------");
        System.out.println("Bezahlt: " +this.selected);
        System.out.println();
    }

    private void exit(){
        System.exit(0);
    }

    /**
     * Add a new Article interactiveley
     * @throws NumberFormatException
     */
    public void artikelAnlegen() throws NumberFormatException{
        System.out.println("---Neuen Artikel Anlegen(leere/ungültige Eingabe bricht ab)---");

        String name = getEntry("Artikelname: ");
        if (name == null){
            return;
        }
        String preis = getEntry("Preis: ");
        if (preis == null) {
            return;
        }
        Double price = Double.parseDouble(preis);
        String liefer = getEntry("Lieferzeit in Tagen: ");
        if (liefer == null) {
            return;
        }
        int deliverydays = Integer.parseInt(liefer);
        addEntry(new BasisArtikel(name, price, deliverydays));

        return;
    }

    /**
     * Select a Article interactively
     * @return Artikel that was selected
     */
    private Artikel selectArticle() throws IndexOutOfBoundsException {
        this.output();
        String nummerString  = getEntry("Wählen sie einen Artikel aus");
        int i= Integer.parseInt(nummerString);
        Artikel a = null;
        if(i-1 < artikelListe.size()) {
            a = artikelListe.get(i - 1);
            resetArtikel(a);
        }
        else{
            throw new IndexOutOfBoundsException();
        }


        return a;
    }

    private void resetArtikel(Artikel a){
        if (a != this.selected) {
            this.selected = a;
            this.express = false;
            this.express = false;
            this.skonto = false;
        }
    }
    /**
     * Add an Artikel to the List
     * @param art
     * @return boolean wether successfull
     */
    private boolean addEntry(Artikel art){
        return this.artikelListe.add(art);
    }
    public String getEntry(String description){
        System.out.print(description);
        String s= scan.nextLine();
        if (s.equals("")){
            return null;
        }
        return s;
    }

    /**
     * Apply a {@link ExpresslieferungDecorator ExpesslieferungDecorator(Artikel artikel)} to passed artikel if not already applied
     * @param a
     * @return
     */
    private Artikel expressLiefern(Artikel a) {
        if (!express && a!=null) {
            a = new ExpresslieferungDecorator(a);
            this.selected = a;
            express= true;
        }
        return a;
    }

    /**
     * Apply a {@link KundenrabattDecorator KundenrabattDecorator(Artikel artikel)} to passed artikel if not already applied
     * @param a
     * @return
     */

    private Artikel kundenRabattAnwenden(Artikel a){
        if (!kundenRabatt && a!=null){
            a = new KundenrabattDecorator(a);
            this.selected = a;
            kundenRabatt = true;
        }
        return a;
    }

    /**
     * Apply a {@link SkontoDecorator SkontoDecorator(Artikel artikel)} to passed artikel if not already applied
     * @param a
     * @return
     */

    private Artikel skontoAnwenden(Artikel a){
        if (!skonto && a!=null){
            a = new SkontoDecorator(a);
            this.selected = a;
            skonto = true;
        }
        return a;
    }
}
