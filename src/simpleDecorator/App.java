package simpleDecorator;

import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;

public abstract class App {
    public static void main(String[] args) {
        Text test = new EinfacherText("Hallo Welt");
        System.out.println(test.ausgeben());
        Text test1 = new GeorgDecorator(new FragezeichenDecorator(new SternDecorator(test)));
       test1 = new SingleCharacterDecorator(test1);
        System.out.println(test1.ausgeben());
        String c;
        c = test1.ausgeben();
        while (c != null) {
            System.out.println(c);
            c = test1.ausgeben();
        }

        test = new FragezeichenDecorator(test);
        //System.out.println(test1.ausgeben());
      /*  Artikel a = new BasisArtikel(100, 5);
        Artikel b = new SkontoDecorator(a);
        Artikel c = new KundenrabattDecorator(a);
        Artikel d = new KundenrabattDecorator(b);
        Artikel e = new ExpresslieferungDecorator(a);

        System.out.println(a.getPreis());
        System.out.println(b.getPreis());
        System.out.println(c.getPreis());
        System.out.println(d.getPreis());
        System.out.println(formatDate(a.getEarliestDeliveryDate()));
        System.out.println(formatDate(e.getEarliestDeliveryDate()));*/
    }

    public static String formatDate(GregorianCalendar date){
        SimpleDateFormat sdf = new SimpleDateFormat();
        return sdf.format(date.getTime());

    }
}
