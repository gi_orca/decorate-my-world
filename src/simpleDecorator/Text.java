package simpleDecorator;

/**
 * Interface Representing a text object
 */
public interface Text {
    String ausgeben();
}
