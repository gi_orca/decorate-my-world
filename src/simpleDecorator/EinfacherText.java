package simpleDecorator;


/**
 * Basic Text object takes a string on construction and has a method to return said string
 */
public class EinfacherText implements Text {
    private String s;

    /**
     * Creates a Basic text
     * @param s the String that text object contains
     */
    public EinfacherText(String s) {
        this.s = s;
    }

    /**
     * Returns the contained Text
     * @return
     */
    @Override
    public String ausgeben() {
        return s;
    }
}
