package simpleDecorator;

/**
 * blueprint for Text deocrators
 */
public abstract class TextDecorator implements Text {
    private Text t;

    public TextDecorator(Text t) {
        this.t = t;
    }
    public String ausgeben() {
        return t.ausgeben();
    }
}
