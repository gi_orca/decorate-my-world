package simpleDecorator;

/**
 * Decorator that appends Georg and a space
 */
public class GeorgDecorator extends TextDecorator {
    /**
     * Creates a new Text Object that builds upon the passed Text objects functionality
     * @param t the Text object to modify
     */
    public GeorgDecorator(Text t) {
        super(t);
    }

    /**
     * Adds Georg in Front of the rest
     * @return
     */
    @Override
    public String ausgeben() {
        return "Georg " + super.ausgeben();
    }
}
