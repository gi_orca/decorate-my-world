package simpleDecorator;

/** Creates a Decorator that returns only a a single character for each
 *
 */
public class SingleCharacterDecorator extends TextDecorator {
    private int pos;

    /**
     *
     * @param t the Text that is to be decorated
     */
    public SingleCharacterDecorator(Text t) {
        super(t);
        this.pos = 0;
    }

    /**
     * Returns a Single character or null if last character has been reached or Object returned a null String of the decorated Object
     * conescutive calls return next character, after char returns null and resets
     * @return String containing single character
     */
    public String ausgeben() {
        String s = super.ausgeben();
        String c = null;
        if (s != null && s.length() > 0 && pos < s.length()) {
            c = "" + s.charAt(pos);
            pos++;
        } else {
            pos = 0;
        }
        return c;
    }
}
