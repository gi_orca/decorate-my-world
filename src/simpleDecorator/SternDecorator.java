package simpleDecorator;

/**
 * Decorator that wraps the given Text in Asterisk and a space
 */
public class SternDecorator extends TextDecorator {

    /**
     * Creates a new Text Object that builds upon the passed Text objects functionality
     * @param t the Text object to modify
     */
    public SternDecorator(Text t) {
        super(t);
    }
    /**
     * Wraps the given Text in Asterisk and a space
     * @return is Wrapped in between ?
     */
    @Override
    public String ausgeben() {
        return "* " + super.ausgeben() + " *";
    }
}
